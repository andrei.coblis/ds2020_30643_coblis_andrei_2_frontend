import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import * as userService from "../services/userService"

class RegisterForm extends Form {
  state = {
    data: { username: "", password: "", role: "", firstName: "", lastName: "", address: "", gender: "", birthDate: "" },
    errors: {}
  };

  schema = {
    username: Joi.string()
      .required()
      .label("Username"),
    password: Joi.string()
      .required()
      .label("Password"),
    role: Joi.string()
      .required()
      .label("Role"),
    firstName: Joi.string()
      .required()
      .label("FirstName"),
    lastName: Joi.string()
      .required()
      .label("LastName"),
    address: Joi.string()
      .required()
      .label("Address"),
    gender: Joi.string()
      .required()
      .label("Gender"),
    birthDate: Joi.date()
      .required()
      .label("BirthDate"),
  };

  doSubmit = async () => {
    try {
      const response = await userService.register(this.state.data);
      window.location = "/login";
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.username = ex.response.data;
        this.setState({ errors });
      }
    }
  };

  render() {
    return (
      <div>
        <h1>Register</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("username", "Username")}
          {this.renderInput("password", "Password", "password")}
          {this.renderInput("role", "Role")}
          {this.renderInput("firstName", "FirstName")}
          {this.renderInput("lastName", "LastName")}
          {this.renderInput("address", "Address")}
          {this.renderInput("gender", "Gender")}
          {this.renderInput("birthDate", "BirthDate")}
          {this.renderButton("Register")}
        </form>
      </div>
    );
  }
}

export default RegisterForm;
