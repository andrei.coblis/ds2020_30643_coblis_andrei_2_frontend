import React, { Component } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import Pagination from "./common/pagination";
import PatientsTable from "./patientsTable";
import { getPatients, deletePatient } from "../services/patientService";
import { paginate } from "../utils/paginate";
import _ from "lodash";
import SearchBox from "./searchBox";
import { getCurrentUser } from "../services/authService";
import SignalRService from "../services/signalR";

class Patients extends Component {
    state = {
        patients: [],
        currentPage: 1,
        pageSize: 4,
        searchQuery: "",
        sortColumn: { path: "username", order: "asc" }
    }

    async componentDidMount() {
        SignalRService.registerReceiveEvent();
        const { data: patients } = await getPatients();
        const user = await getCurrentUser();
        if(user.Role === "caregiver")
        {
          const filteredPatients = patients.filter(p => p.caregiverName === user.Username);
          this.setState({ patients : filteredPatients });          
        }
        else
        {
          this.setState({ patients });
        }
    }

    handleDelete = async patient => {
        const originalPatients = this.state.patients;
        const patients = originalPatients.filter(u => u.id !== patient.id);
        this.setState({ patients });
    
        try {
          await deletePatient(patient.id);
        } catch (ex) {
          if (ex.response && ex.response.status === 404)
            toast.error("This patient has already been deleted.");
    
          this.setState({ patients: originalPatients });
        }
    };

    handlePageChange = page => {
        this.setState({ currentPage: page });
    };

    handleSearch = query => {
        this.setState({ searchQuery: query, currentPage: 1 });
    };

    handleSort = sortColumn => {
        this.setState({ sortColumn });
    };

    getPagedData = () => {
        const {
          pageSize,
          currentPage,
          sortColumn,
          searchQuery,
          patients: allPatients
        } = this.state;
    
        let filtered = allPatients;
        if (searchQuery)
          filtered = allPatients.filter(u =>
            u.username.toLowerCase().startsWith(searchQuery.toLowerCase())
          );
    
        const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);
    
        const patients = paginate(sorted, currentPage, pageSize);
    
        return { totalCount: filtered.length, data: patients };
    };

    render() {
        const { length: count } = this.state.patients;
        const { pageSize, currentPage, sortColumn, searchQuery } = this.state;
        //const { user } = this.props;
    
        if (count === 0) return <p>There are no patients in the database.</p>;
    
        const { totalCount, data: patients } = this.getPagedData();
    
        return (
          <div className="row">
            <div className="col">
              {
                <Link
                  to="/patients/new"
                  className="btn btn-primary"
                  style={{ marginBottom: 20 }}
                >
                  New Patient
                </Link>
              }
              <p>Showing {totalCount} patients in the database.</p>
              <SearchBox value={searchQuery} onChange={this.handleSearch} />
              <PatientsTable
                patients={patients}
                sortColumn={sortColumn}
                onDelete={this.handleDelete}
                onSort={this.handleSort}
              />
              <Pagination
                itemsCount={totalCount}
                pageSize={pageSize}
                currentPage={currentPage}
                onPageChange={this.handlePageChange}
              />
            </div>
          </div>
        );
    }

}
export default Patients;