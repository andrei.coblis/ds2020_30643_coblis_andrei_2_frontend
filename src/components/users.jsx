import React, { Component } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import Pagination from "./common/pagination";
import UsersTable from "./usersTable";
import { getUsers, deleteUser } from "../services/userService";
import { paginate } from "../utils/paginate";
import _ from "lodash";
import SearchBox from "./searchBox";

class Users extends Component {
    state = {
        users: [],
        currentPage: 1,
        pageSize: 4,
        searchQuery: "",
        sortColumn: { path: "username", order: "asc" }
    }

    async componentDidMount() {
        const { data: users } = await getUsers();
        this.setState({ users });
    }

    handleDelete = async user => {
        const originalUsers = this.state.users;
        const users = originalUsers.filter(u => u.userId !== user.userId);
        this.setState({ users });
    
        try {
          await deleteUser(user.userId);
        } catch (ex) {
          if (ex.response && ex.response.status === 404)
            toast.error("This user has already been deleted.");
    
          this.setState({ users: originalUsers });
        }
    };

    handlePageChange = page => {
        this.setState({ currentPage: page });
    };

    handleSearch = query => {
        this.setState({ searchQuery: query, currentPage: 1 });
    };

    handleSort = sortColumn => {
        this.setState({ sortColumn });
    };

    getPagedData = () => {
        const {
          pageSize,
          currentPage,
          sortColumn,
          searchQuery,
          users: allUsers
        } = this.state;
    
        let filtered = allUsers;
        if (searchQuery)
          filtered = allUsers.filter(u =>
            u.username.toLowerCase().startsWith(searchQuery.toLowerCase())
          );
    
        const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);
    
        const users = paginate(sorted, currentPage, pageSize);
    
        return { totalCount: filtered.length, data: users };
    };

    render() {
        const { length: count } = this.state.users;
        const { pageSize, currentPage, sortColumn, searchQuery } = this.state;
        //const { user } = this.props;
    
        if (count === 0) return <p>There are no users in the database.</p>;
    
        const { totalCount, data: users } = this.getPagedData();
    
        return (
          <div className="row">
            <div className="col">
              {
                <Link
                  to="/users/new"
                  className="btn btn-primary"
                  style={{ marginBottom: 20 }}
                >
                  New User
                </Link>
              }
              <p>Showing {totalCount} users in the database.</p>
              <SearchBox value={searchQuery} onChange={this.handleSearch} />
              <UsersTable
                users={users}
                sortColumn={sortColumn}
                onDelete={this.handleDelete}
                onSort={this.handleSort}
              />
              <Pagination
                itemsCount={totalCount}
                pageSize={pageSize}
                currentPage={currentPage}
                onPageChange={this.handlePageChange}
              />
            </div>
          </div>
        );
    }

}
export default Users;