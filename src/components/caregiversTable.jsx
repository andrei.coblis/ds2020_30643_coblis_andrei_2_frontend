import React, { Component } from "react";
//import auth from "../services/authService";
import { Link } from "react-router-dom";
import Table from "./common/table";

class CaregiversTable extends Component {
  columns = [
    {
      path: "username",
      label: "Username",
      content: caregiver => <Link to={`/caregivers/${caregiver.id}`}>{caregiver.username}</Link>
    },
  ];

  deleteColumn = {
    key: "delete",
    content: caregiver => (
      <button
        onClick={() => this.props.onDelete(caregiver)}
        className="btn btn-danger btn-sm"
      >
        Delete
      </button>
    )
  };

  constructor() {
    super();
    //const user = auth.getCurrentUser();
    //if (user && user.isAdmin) this.columns.push(this.deleteColumn);
    this.columns.push(this.deleteColumn);
  }

  render() {
    const { caregivers, onSort, sortColumn } = this.props;

    return (
      <Table
        columns={this.columns}
        data={caregivers}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default CaregiversTable;
