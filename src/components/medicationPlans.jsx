import React, { Component } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import Pagination from "./common/pagination";
import MedicationPlansTable from "./medicationPlansTable";
import { getMedicationPlans, deleteMedicationPlan } from "../services/medicationPlanService";
import { paginate } from "../utils/paginate";
import _ from "lodash";
import SearchBox from "./searchBox";
import { getCurrentUser } from "../services/authService";

class MedicationPlans extends Component {
    state = {
        medicationPlans: [],
        currentPage: 1,
        pageSize: 4,
        searchQuery: "",
        sortColumn: { path: "patientName", order: "asc" }
    }

    async componentDidMount() {
        const { data: medicationPlans } = await getMedicationPlans();
        const user = await getCurrentUser();
        if(user.Role === "patient")
        {
          const filteredMedicationPlans = medicationPlans.filter(m => m.patientName === user.Username)
          this.setState({ medicationPlans : filteredMedicationPlans });
        }
        else
        {
          this.setState({medicationPlans});
        }
    }

    handleDelete = async medicationPlan => {
        const originalMedicationPlans = this.state.medicationPlans;
        const medicationPlans = originalMedicationPlans.filter(u => u.id !== medicationPlan.id);
        this.setState({ medicationPlans });
    
        try {
          await deleteMedicationPlan(medicationPlan.id);
        } catch (ex) {
          if (ex.response && ex.response.status === 404)
            toast.error("This medicationPlan has already been deleted.");
    
          this.setState({ medicationPlans: originalMedicationPlans });
        }
    };

    handlePageChange = page => {
        this.setState({ currentPage: page });
    };

    handleSearch = query => {
        this.setState({ searchQuery: query, currentPage: 1 });
    };

    handleSort = sortColumn => {
        this.setState({ sortColumn });
    };

    getPagedData = () => {
        const {
          pageSize,
          currentPage,
          sortColumn,
          searchQuery,
          medicationPlans: allMedicationPlans
        } = this.state;
    
        let filtered = allMedicationPlans;
        if (searchQuery)
          filtered = allMedicationPlans.filter(u =>
            u.patientName.toLowerCase().startsWith(searchQuery.toLowerCase())
          );
    
        const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);
    
        const medicationPlans = paginate(sorted, currentPage, pageSize);
    
        return { totalCount: filtered.length, data: medicationPlans };
    };

    render() {
        const { length: count } = this.state.medicationPlans;
        const { pageSize, currentPage, sortColumn, searchQuery } = this.state;
        //const { user } = this.props;
    
        if (count === 0) return <p>There are no medication plans in the database.</p>;
    
        const { totalCount, data: medicationPlans } = this.getPagedData();
    
        return (
          <div className="row">
            <div className="col">
              {
                <Link
                  to="/medicationPlans/new"
                  className="btn btn-primary"
                  style={{ marginBottom: 20 }}
                >
                  New Medication Plan
                </Link>
              }
              <p>Showing {totalCount} medicationPlans in the database.</p>
              <SearchBox value={searchQuery} onChange={this.handleSearch} />
              <MedicationPlansTable
                medicationPlans={medicationPlans}
                sortColumn={sortColumn}
                onDelete={this.handleDelete}
                onSort={this.handleSort}
              />
              <Pagination
                itemsCount={totalCount}
                pageSize={pageSize}
                currentPage={currentPage}
                onPageChange={this.handlePageChange}
              />
            </div>
          </div>
        );
    }

}
export default MedicationPlans;