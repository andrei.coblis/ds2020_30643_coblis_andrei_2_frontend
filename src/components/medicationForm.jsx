import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import { getMedication, saveMedication } from "../services/medicationService";

class MedicationForm extends Form {
  state = {
    data: {
      medicationPlanId: "",
      name: "",
      dosage: "",
      sideEffects: ""
    },
    errors: {}
  };

  schema = {
    id: Joi.number(),
    medicationPlanId: Joi.string()
      .required()
      .label("MedicationPlanId"),
    name: Joi.string()
      .required()
      .label("Name"),
    dosage: Joi.string()
      .required()
      .label("Dosage"),
    sideEffects: Joi.string()
        .required()
        .label("SideEffects")
  };

  async populateMedication() {
    try {
      const id = this.props.match.params.id;
      if (id === "new") return;

      const { data: medication } = await getMedication(id);
      this.setState({ data: this.mapToViewModel(medication) });
    } catch (ex) {
      if (ex.response && ex.response.status === 404)
        this.props.history.replace("/not-found");
    }
  }

  async componentDidMount() {
    await this.populateMedication();
  }

  mapToViewModel(medication) {
    return {
      id: medication.id,
      medicationPlanId: medication.medicationPlanId,
      name: medication.name,
      dosage: medication.dosage,
      sideEffects: medication.sideEffects
    };
  }

  doSubmit = async () => {
    await saveMedication(this.state.data);

    this.props.history.push("/Medications");
  };

  render() {
    return (
      <div>
        <h1>Medication Form</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("medicationPlanId", "MedicationPlanId")}
          {this.renderInput("name", "name")}
          {this.renderInput("dosage", "Dosage")}
          {this.renderInput("sideEffects", "SideEffects")}
          {this.renderButton("Save")}
        </form>
      </div>
    );
  }
}

export default MedicationForm;
