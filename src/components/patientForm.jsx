import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import { getPatient, savePatient } from "../services/patientService";

class PatientForm extends Form {
  state = {
    data: {
      userId: "",
      caregiverId: "",
      medicalRecord: "",
    },
    errors: {}
  };

  schema = {
    id: Joi.number(),
    userId: Joi.string()
      .required()
      .label("UserId"),
    caregiverId: Joi.string()
      .required()
      .label("CaregiverId"),
    medicalRecord: Joi.string()
      .required()
      .label("MedicalRecord"),
  };

  async populatePatient() {
    try {
      const id = this.props.match.params.id;
      if (id === "new") return;

      const { data: patient } = await getPatient(id);
      this.setState({ data: this.mapToViewModel(patient) });
    } catch (ex) {
      if (ex.response && ex.response.status === 404)
        this.props.history.replace("/not-found");
    }
  }

  async componentDidMount() {
    await this.populatePatient();
  }

  mapToViewModel(patient) {
    return {
      id: patient.id,
      userId: patient.userId,
      caregiverId: patient.caregiverId,
      medicalRecord: patient.medicalRecord,
    };
  }

  doSubmit = async () => {
    await savePatient(this.state.data);

    this.props.history.push("/patients");
  };

  render() {
    return (
      <div>
        <h1>Patient Form</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("userId", "UserId")}
          {this.renderInput("caregiverId", "CaregiverId")}
          {this.renderInput("medicalRecord", "MedicalRecord")}
          {this.renderButton("Save")}
        </form>
      </div>
    );
  }
}

export default PatientForm;
