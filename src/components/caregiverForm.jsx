import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import { getCaregiver, saveCaregiver } from "../services/caregiverService";

class CaregiverForm extends Form {
  state = {
    data: {
      userId: ""
    },
    errors: {}
  };

  schema = {
    id: Joi.number(),
    userId: Joi.string()
      .required()
      .label("UserId")
  };

  async populateCaregiver() {
    try {
      const id = this.props.match.params.id;
      if (id === "new") return;

      const { data: caregiver } = await getCaregiver(id);
      this.setState({ data: this.mapToViewModel(caregiver) });
    } catch (ex) {
      if (ex.response && ex.response.status === 404)
        this.props.history.replace("/not-found");
    }
  }

  async componentDidMount() {
    await this.populateCaregiver();
  }

  mapToViewModel(caregiver) {
    return {
      id: caregiver.id,
      userId: caregiver.userId
    };
  }

  doSubmit = async () => {
    await saveCaregiver(this.state.data);

    this.props.history.push("/Caregivers");
  };

  render() {
    return (
      <div>
        <h1>Caregiver Form</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("userId", "UserId")}
          {this.renderButton("Save")}
        </form>
      </div>
    );
  }
}

export default CaregiverForm;
