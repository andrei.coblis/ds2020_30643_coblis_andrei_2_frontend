import React, { Component } from "react";
//import auth from "../services/authService";
import { Link } from "react-router-dom";
import Table from "./common/table";

class PatientsTable extends Component {
  columns = [
    {
      path: "username",
      label: "Username",
      content: patient => <Link to={`/patients/${patient.id}`}>{patient.username}</Link>
    },
    { path: "caregiverName", label: "CaregiverName" },
    { path: "medicalRecord", label: "MedicalRecord" },
  ];

  deleteColumn = {
    key: "delete",
    content: patient => (
      <button
        onClick={() => this.props.onDelete(patient)}
        className="btn btn-danger btn-sm"
      >
        Delete
      </button>
    )
  };

  constructor() {
    super();
    this.columns.push(this.deleteColumn);
  }

  render() {
    const { patients, onSort, sortColumn } = this.props;

    return (
      <Table
        columns={this.columns}
        data={patients}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default PatientsTable;
