import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/doctors";

function doctorUrl(id) {
  return `${apiEndpoint}/${id}`;
}

export function getDoctors() {
  return http.get(apiEndpoint);
}

export function getDoctor(id) {
  return http.get(doctorUrl(id));
}

export function saveDoctor(doctor) {
  if (doctor.id) {
    const body = { ...doctor };
    delete body.id;
    return http.put(doctorUrl(doctor.id), body);
  }

  return http.post(apiEndpoint, doctor);
}

export function deleteDoctor(id) {
  return http.delete(doctorUrl(id));
}
