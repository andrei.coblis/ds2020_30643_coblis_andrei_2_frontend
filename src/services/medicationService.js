import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/medications";

function medicationUrl(id) {
  return `${apiEndpoint}/${id}`;
}

export function getMedications() {
  return http.get(apiEndpoint);
}

export function getMedication(id) {
  return http.get(medicationUrl(id));
}

export function saveMedication(medication) {
  if (medication.id) {
    const body = { ...medication };
    delete body.id;
    return http.put(medicationUrl(medication.id), body);
  }

  return http.post(apiEndpoint, medication);
}

export function deleteMedication(id) {
  return http.delete(medicationUrl(id));
}
